package co.vulpin.aoc

import co.vulpin.aoc.misc.Timer

import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors

abstract class Day<E> {


    static exec = Executors.newCachedThreadPool()

    CompletableFuture part1 = new CompletableFuture(), part2 = new CompletableFuture()

    protected String input
    protected E parsed

    Day() {
        input = readInput()
        parsed = parse()
    }

    final void run() {

        def timer = new Timer()

        def locale = "%s%-10s%s%-10s%s"

        timer.start()

        exec.submit{ calculate() }

        printf(locale, "Part 1: ", part1.get(), "    ", timer.formatDuration(), "\n")
        printf(locale, "Part 2: ", part2.get(), "    ", timer.formatDuration(), "\n")

        exec.shutdown()

    }

    void calculate() {
        exec.submit{
            try {
                part1.complete(part1())
            } catch (Exception e) {
                e.printStackTrace()
            }
        }
        exec.submit{
            try {
                part2.complete(part2())
            } catch (Exception e) {
                e.printStackTrace()
            }
        }
    }

    def part1() {}
    def part2() {}

    private String readInput() {
        def path = "src/" + this.class.package.name.replace(".", "/") + "/input.txt"
        def file = new File(path)
        return file.exists() ? file.text : null
    }

    protected E parse() {
        return input as E
    }

}
