package co.vulpin.aoc.misc

class CircularList<E> extends LinkedList<E> {

    CircularList() {
        super()
    }

    CircularList(Collection<? extends E> existing) {
        super(existing)
    }

    @Override
    E get(int index) {
        return super.get(fixIndex(index)) as E
    }

    @Override
    E set(int index, E element) {
        return super.set(fixIndex(index), element) as E
    }

    @Override
    void add(int index, E element) {
        super.add(fixIndex(index), element)
    }

    private int fixIndex(int index) {
        return index % size()
    }

}
