package co.vulpin.aoc.misc

import java.text.NumberFormat

class Timer {

    private static final NumberFormat formatter = NumberFormat.instance

    private long start, stop

    void start() {
        start = System.currentTimeMillis()
    }

    void stop() {
        stop = System.currentTimeMillis()
    }

    long getDuration() {
        stop()
        return stop - start
    }

    String formatDuration() {
        return formatter.format(Double.valueOf(duration) / 1000) + "s"
    }

}
