package co.vulpin.aoc.misc

import groovy.transform.Memoized

class KnotHash {

    @Memoized
    static String knot(String input) {
        def nums = new ArrayList(0..255)
        def lengths = input.chars.collect{ it as int } + [17,31,73,47,23]
        def times = 64

        knot(nums, lengths, times)

        return hex(nums)
    }

    static void knot(List<Integer> nums, List<Integer> lengths, int times) {
        def cur = 0
        def skip = 0

        times.times {
            for(int length : lengths) {
                reverse(nums, cur, length)
                cur += length + skip
                skip++
            }
        }
    }

    static String hex(List<Integer> nums) {
        def reduced = reduce(nums)

        return reduced.collect{ return String.format("%02X", it) }
                .join("")
                .toLowerCase()
    }

    static List<Integer> reduce(List<Integer> nums) {
        return nums.collate(nums.size() / 16 as int)
                .collect {
            int cur = it.first()
            for(int i = 1; i < it.size(); i++) {
                cur ^= it.get(i)
            }
            return cur
        }
    }

    static void reverse(List<Integer> nums, int start, int length) {
        def indexes = [] as List<Integer>
        for(int i = 0; i < length; i++) {
            indexes += (start + i) % nums.size()
        }
        def selection = indexes.collect{ nums.get(it) }
                .reverse()
        for(int i = 0; i < indexes.size(); i++) {
            nums.set(indexes.get(i), selection.get(i))
        }
    }

}
