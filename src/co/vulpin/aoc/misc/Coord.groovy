package co.vulpin.aoc.misc

class Coord {

    int x, y

    Coord(x, y) {
        this.x = x
        this.y = y
    }

    @Override
    boolean equals(Object other) {
        if(other instanceof Coord) {
            return x == other.x && y == other.y
        }
        return false
    }

    @Override
    String toString() {
        return "[$x,$y]"
    }

}
