package co.vulpin.aoc

class Main {

    static void main(String[] args) {

        def cal = Calendar.instance

        def dayNum = -1
        def yearNum = -1

        switch(args.length) {
            case 2:
                dayNum = args[1] as int
                yearNum = args[0] as int
                break
            case 1:
                dayNum = args[0] as int
                yearNum = cal.get(Calendar.YEAR)
                break
            case 0:
                dayNum = cal.get(Calendar.DAY_OF_MONTH)
                yearNum = cal.get(Calendar.YEAR)
        }

        def dayText = String.format("%02d", dayNum)

        def dayClass = Class.forName("co.vulpin.aoc.year${yearNum}.day${dayText}.Day${dayText}")

        def day = dayClass.newInstance() as Day

        println "$yearNum - Day $dayNum\n"
        day.run()

    }

}
