package co.vulpin.aoc.year2017.day21

import co.vulpin.aoc.Day

class Day21 extends Day<List<Rule>> {

    def part1() {

        def board = createStandardBoard()

        5.times {

        }

    }

    def part2() {

    }

    boolean[][] createStandardBoard() {
        return [
            [false, true,  false],
            [false, false, true],
            [true,  true,  true]
        ]
    }

    List<Rule> parse() {
        return input.split("\n").collect{
            def parts = it.split(" => ")
            def input = parseToBoard(parts[0])
            def r90 =  rotate90(input)
            def r180 = rotate90(r90)
            def r270 = rotate90(r180)
            def flipped = flip(input)
            def output = parseToBoard(parts[1])
            return [
                    input,
                    r90,
                    r180,
                    r270,
                    flipped
            ].collect { new Rule(it, output) }
        }.flatten() as List<Rule>
    }




}
