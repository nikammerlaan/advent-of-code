package co.vulpin.aoc.year2017.day21

class Rule {

    Board input
    Board output

    Rule(boolean[][] input, boolean[][] output) {
        this.input = new Board(input)
        this.output = new Board(output)
    }

    void apply(boolean[][] board) {}

    boolean matches(boolean[][] board) {
        def len = board.length

        if(len != input.length) return false

        for(int row = 0; row < len; row++) {
            for(int col  = 0; col < len; col++) {
                if(board[row][col] != input[row][col]) return false
            }
        }

        return true
    }



}
