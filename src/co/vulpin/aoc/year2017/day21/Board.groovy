package co.vulpin.aoc.year2017.day21

class Board {

    boolean[][] values

    Board(String text) {
        this(parseToBoard(text))
    }

    Board(boolean[][] values) {
        this.values = values
    }

    boolean matches() {

    }

    int getLen() {
        return values.length
    }

    Board divide() {
        def factor = 2
        while(len % 2 > 0) factor++
        def amount = len / factor


        for(int row = 0; row < amount; row++) {
            for(int col = 0; col < amount; col++) {

            }
        }
    }

    boolean[][] flip() {
        boolean[][] flipped = new boolean[len][]
        for(int i = 0; i < len; i++) {
            flipped[i] = flipArray(values[i])
        }
        return flipped
    }

    boolean[][] rotate90() {
        boolean[][] rotated = new boolean[len][len]
        len.times { row ->
            len.times { col ->
                rotated[col][len - row - 1] = values[row][col]
            }
        }
        return rotated
    }

    static boolean[] flipArray(boolean[] array) {
        def len = array.length
        boolean[] flipped = new boolean[len]
        for(int i = 0; i < len; i++)
            flipped[len - i - 1] = array[i]
        return flipped
    }

    private static boolean[][] parseToBoard(String input) {
        def parts = input.split("/")
        def len = parts.length
        boolean[][] board = new boolean[len][len]
        parts.eachWithIndex{ String line, int row ->
            line.trim().chars.eachWithIndex{ char character, int col ->
                board[row][col] = character == "#"
            }
        }
        return board
    }

}
