package co.vulpin.aoc.year2017.day09

import groovy.transform.Memoized

import java.util.concurrent.LinkedBlockingQueue

class Group {

    String internal
    int score

    Group(int score, String internal) {
        this.score = score
        this.internal = internal
    }

    @Memoized
    List<Group> getChildren() {
        def parts = getParts(internal)
        return parts.collect{ new Group(score + 1, it) }
    }

    int getTotalScore() {
        def childrenScore = 0
        if(children) {
            childrenScore = children.collect{ it.totalScore }.sum() as int
        }
        return score + childrenScore
    }

    @Memoized
    static List<String> getParts(String text) {
        // removes the {} around the edges
        text = text.substring(1, text.length() - 1)

        // gets every char and then puts it into a queue
        def chars = new LinkedBlockingQueue<Character>(Arrays.asList(text.chars) as List<Character>)

        def parts = []

        def cur = ""
        while(!chars.isEmpty()) {
            cur += chars.take()

            def left = cur.count("{")
            def right = cur.count("}")

            if(left == 0 && right == 0) {
                cur = ""
            } else if(left == right) {
                parts += cur
                cur = ""
            }
        }

        return parts
    }

}
