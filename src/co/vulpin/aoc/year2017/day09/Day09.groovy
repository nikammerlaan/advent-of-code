package co.vulpin.aoc.year2017.day09

import co.vulpin.aoc.Day

class Day09 extends Day<Group> {

    def part1() {
        return parsed.totalScore
    }

    def part2() {
        return input.replaceAll("!.", "")
                   .findAll("(?<=<).*?(?=>)")
                   .sum{it.length()}
    }

    @Override
    Group parse() {
        def parsed = input.replaceAll("!.", "")
                         .replaceAll("<.*?>", "")

        return new Group(1, parsed)
    }



}
