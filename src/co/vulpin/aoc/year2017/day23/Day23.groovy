package co.vulpin.aoc.year2017.day23

import co.vulpin.aoc.Day
import co.vulpin.aoc.year2017.day18.Part1Program

class Day23 extends Day<String[][]> {

    def part1() {
        def mul = 0
        def prog = new Part1Program(parsed) {
            void mul(String reg, String amount) {
                super.mul(reg, amount)
                mul++
            }
        }

        while(!prog.isFinished()) prog.step()

        return mul
    }

    def part2() {
        int b,c,d,f,g,h
        b=c=d=f=g=h=0

        b = 99 * 100 + 100_000
        c = b + 17_000

        while(true) {
            for(d = 2; d < b; d++) {
                if(b % d == 0) {
                    h++
                    break
                }
            }

            g = b - c
            b += 17

            if(g == 0) break
        }

        return h
    }

    @Override
    String[][] parse() {
        return input.split("\n").collect{ it.split("\\s+") }
    }

}
