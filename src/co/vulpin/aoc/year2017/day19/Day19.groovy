package co.vulpin.aoc.year2017.day19

import co.vulpin.aoc.Day
import co.vulpin.aoc.misc.Coord
import groovy.transform.Memoized

import static Direction.*

class Day19 extends Day<char[][]> {

    @Override
    void calculate() {
        def cur = findStart()
        Direction direction = SOUTH

        def letters = []
        def steps = 0

        while(true) {
            steps++

            if(!canMove(cur, direction)) {
                def directions = values().findAll{ it != direction && it != direction.opposite}
                direction = directions.find{ canMove(cur, it) }
            }

            if(!direction) break

            cur = move(cur, direction)

            def value = getValue(cur)
            if(value.isLetter()) letters += value

        }

        part1.complete(letters.join(""))
        part2.complete(steps)
    }

    boolean canMove(Coord coord, Direction direction) {
        return hasValue(move(coord, direction))
    }

    Coord move(Coord coord, Direction direction) {
        def x = coord.x, y = coord.y

        switch(direction) {
            case NORTH:
                y--
                break
            case SOUTH:
                y++
                break
            case EAST:
                x++
                break
            case WEST:
                x--
                break
        }

        def newCoord = new Coord(x, y)
        return newCoord
    }

    Coord findStart() {
        def coord = new Coord(0, 0)

        while(getValue(coord) != "|") coord.x++

        return coord
    }

    boolean hasValue(Coord coord) {
        return !getValue(coord).isWhitespace()
    }

    Character getValue(Coord coord) {
        return getValue(coord.x, coord.y)
    }

    @Memoized
    Character getValue(int x, int y) {
        if(y >= 0 && y < parsed.length) {
            def row = parsed[y]
            if(x >= 0 && x < row.length) {
                return parsed[y][x]
            }
        }
        return ' '.charAt(0)
    }

    char[][] parse() {
        def lines = input.split("\n")

        char[][] chars = new char[lines.size()][]

        lines.eachWithIndex { line, i ->
            chars[i] = line.chars
        }

        return lines
    }



}
