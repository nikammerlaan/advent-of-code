package co.vulpin.aoc.year2017.day19

import groovy.transform.Memoized

enum Direction {

    NORTH,
    SOUTH,

    EAST,
    WEST

    @Memoized
    Direction getOpposite() {
        switch(this) {
            case NORTH: return SOUTH
            case SOUTH: return NORTH
            case EAST:  return WEST
            case WEST:  return EAST
        }
    }

}
