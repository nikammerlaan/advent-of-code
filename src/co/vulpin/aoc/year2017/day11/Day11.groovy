package co.vulpin.aoc.year2017.day11

import co.vulpin.aoc.Day
import groovy.transform.Memoized

class Day11 extends Day<String[]> {

    void calculate() {
        def x = 0, y = 0, z = 0

        def distance = 0, biggest = 0

        parse().each {
            switch (it){
                case "n":  x++; break
                case "s":  x--; break

                case "ne": y++; break
                case "sw": y--; break

                case "nw": z++; break
                case "se": z--; break
            }

            distance = getDistance(x, y, z)
            if(distance > biggest) biggest = distance
        }

        part1.complete(distance)
        part2.complete(biggest)
    }

    @Memoized
    static int getDistance(int x, int y, int z) {
        x = Math.abs(x)
        y = Math.abs(y)
        z = Math.abs(z)

        return x + Math.max(y, z)
    }

    @Override
    String[] parse() {
        return input.split(",")
    }

}