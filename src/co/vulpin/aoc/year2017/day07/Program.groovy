package co.vulpin.aoc.year2017.day07

import groovy.transform.Memoized

class Program {

    Map<String, Program> map

    String name
    int weight
    List<String> childrenNames

    Program(Map<String, Program> map, String name, int weight, List<String> childrenNames) {
        this.map = map
        this.name = name
        this.weight = weight
        this.childrenNames = childrenNames
    }

    @Memoized
    List<Program> getChildren() {
        return childrenNames.collect{ map.get(it) }
    }

    @Memoized
    int getTotalWeight() {
        int childrenWeight = children.collect{ it.totalWeight }.sum() as Integer ?: 0
        return weight + childrenWeight
    }

    String toString() {
        return "$name, $weight, $totalWeight, $childrenNames"
    }

}
