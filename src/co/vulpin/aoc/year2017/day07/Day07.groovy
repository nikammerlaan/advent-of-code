package co.vulpin.aoc.year2017.day07

import co.vulpin.aoc.Day

class Day07 extends Day<Map<String, Program>> {

    def part1() {
        return parsed.values().max{ it.totalWeight }.name
    }

    def part2() {
        def unbalanced = parsed.values().findAll {
            def weights = it.children.collect{ it.totalWeight }
            def unique = weights.unique()
            return unique.size() > 1
        }.min{ it.totalWeight }

        def occurrences = [:]
        unbalanced.children.each {
            def total = it.totalWeight
            occurrences.put(total, occurrences.getOrDefault(total, 0) + 1)
        }

        def target = occurrences.max{ it.value }.key
        def oddOneOut = occurrences.min{ it.value }.key
        def diff = target - oddOneOut
        def node = unbalanced.children.find{ it.totalWeight == oddOneOut }
        return node.weight + diff
    }

    Map<String, Program> parse() {
        def map = [:]
        input.split("\n").each { line ->
            def parts = line.split("[->, ()]").collect{it.trim()}.findAll{it}
            def name = parts[0]
            def weight = parts[1].toInteger()
            def children = []
            if(parts.size() > 2) {
                for(int i = 2; i < parts.size(); i++) {
                    children += parts[i]
                }
            }
            def prog = new Program(map, name, weight, children)
            map.put(name, prog)
        }
        return map
    }

}
