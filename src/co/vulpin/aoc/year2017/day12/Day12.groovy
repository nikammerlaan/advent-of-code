package co.vulpin.aoc.year2017.day12

import co.vulpin.aoc.Day
import groovy.transform.Memoized

import java.util.concurrent.LinkedBlockingQueue

class Day12 extends Day<Map<Integer, List<Integer>>> {

    def part1() {
        return findConnected(0).size()
    }

    def part2() {
        def seen = new HashSet<>()
        def groups = 0
        for(Integer item : parsed.keySet()) {
            if(!seen.contains(item)) {
                groups++
                seen.addAll(findConnected(item))
            }
        }

        return groups
    }

    @Memoized
    HashSet<Integer> findConnected(int start) {
        def queue = new LinkedBlockingQueue<Integer>()
        queue.addAll(parsed.get(start))

        def seen = new HashSet<Integer>()
        seen.add(start)

        while(queue.size() > 0) {
            def item = queue.take()

            seen.add(item)

            def unseen = parsed.get(item).findAll{ !seen.contains(it) }
            queue.addAll(unseen)
        }

        return seen
    }

    @Override
    Map<Integer, List<Integer>> parse() {
        def map = [:]
        input.split("\n").each {
            def parts = it.findAll("[0-9]+")
            def conn = parts.size() > 1 ? parts.subList(1, parts.size()) : []
            map.put(parts[0] as int, conn.collect{it as int})
        }
        return map
    }

}
