package co.vulpin.aoc.year2017.day24

class Bridge implements Cloneable {

    int length, strength, unpaired

    Bridge(Component component) {
        add(component)
    }

    boolean canConnect(Component component) {
        return component.left == unpaired || component.right == unpaired
    }

    void add(Component component) {
        length++
        strength += component.strength
        unpaired = (component.left == unpaired) ? component.right : component.left
    }

}
