package co.vulpin.aoc.year2017.day24

import groovy.transform.Memoized

class Component {

    int left, right

    Component(Integer left, Integer right) {
        this.left = left
        this.right = right
    }

    int getStrength() {
        return left + right
    }

    @Memoized
    boolean canConnect(Component component) {
        return left == component.left || left == component.right || right == component.left || right == component.right
    }

    String toString() {
        return "$left/$right"
    }

}
