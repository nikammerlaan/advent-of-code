package co.vulpin.aoc.year2017.day24

import co.vulpin.aoc.Day

import java.util.concurrent.Executors

class Day24 extends Day<List<Component>> {

    @Override
    void calculate() {
        def possible = parsed.findAll{ it.left == 0 }

        def bridges = possible.collect { buildBridges(new Bridge(it), parsed - it) }
                              .flatten() as List<Bridge>

        part1.complete(bridges.max{ it.strength }.strength)

        def max = bridges.max{ it.length }.length
        def maxBridges = bridges.findAll{ it.length == max }

        part2.complete(maxBridges.max{ it.strength }.strength)
    }

    List<Bridge> buildBridges(Bridge bridge, List<Component> options) {
        def can = options.findAll{ bridge.canConnect(it) }

        if(!can) return [bridge]

        def bridges = can.collect {
            def copy = bridge.clone() as Bridge
            copy.add(it)
            return buildBridges(copy, options - it)
        }.flatten() as List<Bridge>

        return bridges
    }


    List<Component> parse() {
        return input.split("\n").collect{
            def parts = it.split("/")
            return new Component(parts[0] as int, parts[1] as int)
        }
    }

}
