package co.vulpin.aoc.year2017.day05

import co.vulpin.aoc.Day

class Day05 extends Day<Integer[]> {

    def part1() {
        calculate(false)
    }

    def part2() {
        calculate(true)
    }

    int calculate(boolean part2) {
        def array = Arrays.copyOf(parsed, parsed.length)
        def index = 0
        def step = 0
        while(index < array.length) {
            def value = array[index]
            def change = value >= 3 && part2 ? -1 : 1
            array[index] = value + change
            index += value
            step++
        }
        return step
    }

    Integer[] parse() {
        return input.split("\n").collect{ it.toInteger() }
    }

}
