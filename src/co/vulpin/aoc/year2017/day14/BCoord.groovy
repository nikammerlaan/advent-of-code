package co.vulpin.aoc.year2017.day14

import co.vulpin.aoc.misc.Coord

class BCoord extends Coord {

    boolean occupied

    BCoord(int x, int y, boolean occupied) {
        super(x, y)
        this.occupied = occupied
    }

}
