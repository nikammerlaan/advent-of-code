package co.vulpin.aoc.year2017.day14

import co.vulpin.aoc.Day
import co.vulpin.aoc.misc.KnotHash

import java.util.concurrent.LinkedBlockingQueue

class Day14 extends Day<BCoord[][]> {

    def part1() {
        def sum = 0
        parsed.each {
            it.each {
                if(it.isOccupied()) sum++
            }
        }
        return sum
    }

    def part2() {
        def seen = new HashSet<>()
        int groups = 0

        parsed.each {
            it.each {
                if(!seen.contains(it) && it.isOccupied()) {
                    seen.addAll(getRegion(it))
                    groups++
                }
            }
        }

        return groups
    }

    HashSet<BCoord> getRegion(BCoord coord) {
        def seen = new HashSet<BCoord>()
        def queue = new LinkedBlockingQueue<BCoord>()
        queue.add(coord)

        while(!queue.isEmpty()) {
            def item = queue.poll()
            getAdjacent(item).each {
                if(!seen.contains(it) && it.isOccupied()) {
                    queue.add(it)
                    seen.add(it)
                }
            }
        }

        return seen
    }

    List<BCoord> getAdjacent(BCoord coord) {
        int x = coord.x, y = coord.y

        println "$x, $y"

        def coords = []

        if(x < 127)
            coords += parsed[x + 1][y]
        if(x > 0)
            coords += parsed[x - 1][y]
        if(y < 127)
            coords += parsed[x][y + 1]
        if(y > 0)
            coords += parsed[x][y - 1]

        return coords
    }

    static String bin(String hex) {
        return hex.collect {
            def integer = Integer.parseInt(it.toString(), 16)
            def binary = Integer.toBinaryString(integer)
            binary = ("0" * (4 - binary.size())) + binary
            return binary
        }.join("")
    }

    @Override
    BCoord[][] parse() {
        BCoord[][] array = new BCoord[128][]
        (0..127).each {
            def input = "$input-$it"
            def hex = KnotHash.knot(input)
            def binary = bin(hex)
            def row = new BCoord[128]
            for(int i = 0; i < row.length; i++) {
                def value = binary.charAt(i).toString() == '1'
                row[i] = new BCoord(it, i, value)
            }
            array[it] = row
        }
        return array
    }

}
