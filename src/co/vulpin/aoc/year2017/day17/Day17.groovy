package co.vulpin.aoc.year2017.day17

import co.vulpin.aoc.Day

class Day17 extends Day<Integer> {

    def part1() {
        def nums = [0]

        def pos = 0
        for(int i = 1; i <= 2017; i++) {
            def index = (pos + parsed) % i + 1
            pos = index
            nums.add(index, i)
        }

        return nums[nums.indexOf(2017) + 1]
    }

    def part2() {
        def pos = 0
        def last = 0

        for(int i = 1; i <= 50_000_000; i++) {
            def index = (pos + parsed) % i + 1
            pos = index
            if(index == 1) last = i
        }

        return last
    }

    Integer parse() {
        input as int
    }

}
