package co.vulpin.aoc.year2017.day18

class Part2Program extends Program {

    Queue<Long> send, receive

    int sent = 0

    boolean stepped = false

    Part2Program(String[][] lines, int id, Queue<Long> send, Queue<Long> receive) {
        super(lines)
        map.put("p", id)
        this.send = send
        this.receive = receive
    }

    @Override
    void step() {
        stepped = true
        super.step()
    }

    @Override
    void rcv(String reg) {
        if(!receive.isEmpty()) {
            map[reg] = receive.poll()
        } else {
            stepped = false
            index--
        }
    }

    @Override
    void snd(String amount) {
        send.add(resolve(amount))
        sent++
    }
}
