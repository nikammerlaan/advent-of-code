package co.vulpin.aoc.year2017.day18

abstract class Program {

    Map<String, Long> map = [:]
    String[][] lines
    int index = 0

    Program(String[][] lines) {
        this.lines = lines
    }

    boolean isFinished() {
        return index < 0 || index >= lines.length
    }

    void step() {
        def parts = lines[index]
        switch(parts[0]) {
            case "set": set(parts[1], parts[2]); break
            case "add": add(parts[1], parts[2]); break
            case "sub": sub(parts[1], parts[2]); break
            case "mul": mul(parts[1], parts[2]); break
            case "mod": mod(parts[1], parts[2]); break
            case "jgz": jgz(parts[1], parts[2]); break
            case "jnz": jnz(parts[1], parts[2]); break
            case "rcv": rcv(parts[1]); break
            case "snd": snd(parts[1]); break
        }
        index++
    }

    void set(String reg, String amount) {
        map[reg] = resolve(amount)
    }

    void add(String reg, String amount) {
        map[reg] = resolve(reg) + resolve(amount)
    }

    void sub(String reg, String amount) {
        map[reg] = resolve(reg) - resolve(amount)
    }

    void mul(String reg, String amount) {
        map[reg] = resolve(reg) * resolve(amount)
    }

    void mod(String reg, String amount) {
        map[reg] = resolve(reg) % resolve(amount)
    }

    void jgz(String reg, String amount) {
        if(resolve(reg) > 0) index += resolve(amount) - 1
    }

    void jnz(String reg, String amount) {
        if(resolve(reg) != 0) index += resolve(amount) - 1
    }

    abstract void rcv(String reg)

    abstract void snd(String amount)

    Long resolve(String input) {
        return input.isLong() ? input as long : map.getOrDefault(input, 0)
    }

}
