package co.vulpin.aoc.year2017.day18

import groovy.transform.InheritConstructors

@InheritConstructors
class Part1Program extends Program {

    int lastFreq
    boolean recovered = false

    @Override
    void rcv(String reg) {
        if(resolve(reg) > 0) recovered = true
    }

    @Override
    void snd(String amount) {
        lastFreq = resolve(amount)
    }

}
