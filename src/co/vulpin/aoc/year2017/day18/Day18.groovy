package co.vulpin.aoc.year2017.day18

import co.vulpin.aoc.Day

import java.util.concurrent.LinkedBlockingQueue

class Day18 extends Day<String[][]> {

    def part1() {
        def prog = new Part1Program(parsed)
        while(!prog.isFinished() && !prog.recovered) prog.step()
        return prog.lastFreq
    }

    def part2() {
        def queueA = new LinkedBlockingQueue()
        def queueB = new LinkedBlockingQueue()

        def prog0 = new Part2Program(parsed, 0, queueA, queueB)
        def prog1 = new Part2Program(parsed, 1, queueB, queueA)

        while(true) {
            prog0.step()
            prog1.step()

            if(!prog0.stepped && !prog1.stepped) break
        }

        return prog1.sent
    }

    @Override
    String[][] parse() {
        return input.split("\n")
    }
}
