package co.vulpin.aoc.year2017.day08

import co.vulpin.aoc.Day

import java.util.function.BiFunction
import java.util.function.BiPredicate

class Day08 extends Day<String[]> {

    static Map comparators = [
            "==": { x, y -> x == y },
            "!=": { x, y -> x != y },
            ">":  { x, y -> x >  y },
            "<":  { x, y -> x <  y },
            ">=": { x, y -> x >= y },
            "<=": { x, y -> x <= y },
    ]

    static Map actions = [
            "inc": { x, y -> x + y },
            "dec": { x, y -> x - y }
    ]

    void calculate() {

        Map<String, Integer> map = [:]

        int max = Integer.MIN_VALUE

        input.split("\n").each {
            def tokens = it.split("\\s+")


            // the first part of the comparison, the register
            def a = map.getOrDefault(tokens[4], 0)
            // the second part of the comparison, just an int
            def b = tokens[6] as int

            def comparatorString = tokens[5]
            def comparator = comparators.get(comparatorString) as BiPredicate

            if(comparator.test(a, b)) {
                def register = tokens[0]
                def registerVal = map.getOrDefault(register, 0)
                // the action we are going to do to the register value
                def action = actions.get(tokens[1]) as BiFunction
                // the amount that register value is going to be modified by
                def amount = tokens[2] as int

                def result = action.apply(registerVal, amount) as int

                if(result > max) max = result
                map.put(register, result)
            }

        }

        part1.complete(map.collect{ it.value }.max())
        part2.complete(max)

    }

}
