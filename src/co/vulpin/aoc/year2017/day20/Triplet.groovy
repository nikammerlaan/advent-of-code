package co.vulpin.aoc.year2017.day20

class Triplet {

     long x, y, z

    Triplet(int x, int y, int z) {
        this.x = x
        this.y = y
        this.z = z
    }

    Triplet(List<Integer> ints) {
        this(ints[0], ints[1], ints[2])
    }

    void add(Triplet triplet) {
        x += triplet.x
        y += triplet.y
        z += triplet.z
    }

    int getAbsoluteTotal() {
        return Math.abs(x) + Math.abs(y) + Math.abs(z)
    }

    @Override
    boolean equals(Object o) {
        if(o instanceof Triplet) {
            return x == o.x && y == o.y && z == o.z
        }
    }

    String toString() {
        return "[$x,$y,$z]"
    }
}
