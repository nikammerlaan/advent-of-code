package co.vulpin.aoc.year2017.day20

import co.vulpin.aoc.Day

class Day20 extends Day<List<Particle>> {

    def part1() {
        def particles = parse()

        100.times {
            particles.each {
                it.step()
            }
        }

        return particles.indexOf(particles.min{ it.distance })
    }

    def part2() {
        def particles = parse()

        100.times {
            particles.each { it.step() }

            def toRemove = []
            for(Particle particle : particles) {
                if(toRemove.contains(particle)) continue
                def same = particles.findAll{ particle.position == it.position }
                if(same.size() > 1) toRemove.addAll(same)
            }
            particles.removeAll(toRemove)
        }

        return particles.size()
    }

    List<Particle> parse() {
        return input.split("\n").collect {
            def parts = it.findAll("-?\\d+").collect{it as int}.collate(3).collect{ new Triplet(it) }
            return new Particle(parts[0], parts[1], parts[2])
        }
    }

}
