package co.vulpin.aoc.year2017.day20

class Particle {

    Triplet position, velocity, acceleration

    Particle(Triplet position, Triplet velocity, Triplet acceleration) {
        this.position = position
        this.velocity = velocity
        this.acceleration = acceleration
    }

    void step() {
        velocity.add(acceleration)
        position.add(velocity)
    }

    int getDistance() {
        return position.absoluteTotal
    }

    boolean equals(Object o) {
        if(o instanceof Particle) {
            return position == o.position && velocity == o.velocity && acceleration == o.acceleration
        }
    }

    String toString() {
        "p:$position v:$velocity a:$acceleration"
    }

}
