package co.vulpin.aoc.year2017.day01

import co.vulpin.aoc.Day

class Day01 extends Day<String> {

    def part1() {
        return solve(1)
    }

    def part2() {
        return solve(parsed.length() / 2 as int)
    }

    int solve(int dist) {
        def len = parsed.length()
        int sum = 0

        for(int i = 0; i < len; i++) {
            def next = i + dist
            if(next >= len) next -= len

            def a = parsed.charAt(i)
            def b = parsed.charAt(next)

            if(a == b) sum += Character.getNumericValue(a)
        }

        return sum
    }

}
