package co.vulpin.aoc.year2017.day02

import co.vulpin.aoc.Day

class Day02 extends Day<Integer[][]> {

    def part1() {

        def checksum = 0

        for(int[] row : parsed) {

            def highest = Integer.MIN_VALUE
            def lowest = Integer.MAX_VALUE

            for(int i : row) {
                if(i > highest) highest = i
                if(i < lowest)  lowest = i
            }

            checksum += highest - lowest

        }

        return checksum

    }

    def part2() {

        def total = 0

        for(int[] row : parsed) {
            for(int i = 0; i < row.length; i++) {
                def dividend = row[i]
                for(int j = 0; j < row.length; j++ ){
                    if(i == j) continue

                    def divisor = row[j]
                    if(dividend % divisor == 0) {
                        total += dividend / divisor
                    }
                }
            }
        }

        return total

    }

    Integer[][] parse() {
        return input.split("\n").collect{ it.split("\\s++").collect{ it as int } }
    }

}
