package co.vulpin.aoc.year2017.day03

import co.vulpin.aoc.Day
import co.vulpin.aoc.misc.Coord
import groovy.transform.Memoized

class Day03 extends Day<Integer> {

    def part1() {
        def pos = getPos(parsed)
        return pos.with{ Math.abs(x) + Math.abs(y) }
    }

    def part2() {
        def val = 0
        for(int i = 1; val < parsed; i++) {
            val = getValuePart2(i)
        }
        return val
    }

    static List<Coord> getNeighbors(int x, int y) {
        return [
            // right side
            new Coord(x + 1, y),
            new Coord(x + 1, y + 1),
            new Coord(x + 1, y - 1),

            // top/bottom
            new Coord(x, y + 1),
            new Coord(x, y - 1),

            // left side
            new Coord(x - 1, y + 1),
            new Coord(x - 1, y),
            new Coord(x - 1, y - 1),
        ]
    }

    static int getValue(int x, int y) {
        def xAbs = Math.abs(x)
        def yAbs = Math.abs(y)

        if(xAbs > yAbs) {
            if(x > 0) {
                return 4 * Math.pow(x, 2) - 3 * x + y + 1
            } else {
                return 4 * Math.pow(x, 2) - x - y + 1
            }
        } else {
            if(y > 0) {
                return 4 * Math.pow(y, 2)  - y - x + 1
            } else {
                return 4 * Math.pow(y, 2) - 3 * y + x + 1
            }
        }

    }

    @Memoized
    static int getValuePart2(int number) {
        if(number == 1) return 1

        def pos = getPos(number)
        def neighbors = getNeighbors(pos.x, pos.y)
                            .collect{ getValue(it.x, it.y) }
                            .findAll{ it < number }
        return neighbors.collect{
            getValuePart2(it)
        }.sum() as int
    }

    static Coord getPos(int number) {

        def ring = Math.ceil(Math.sqrt(number))
        if(ring % 2 == 0) ring++

        def start = Math.pow(ring - 2, 2)
        def along = number - start
        def side = 0
        def sideLen = ring - 1
        while(along > sideLen) {
            along -= sideLen
            side++
        }

        def half = Math.floor(ring / 2) as int

        int x,y

        switch(side) {
            case 0:
                x = half
                y = -half + along
                break
            case 1:
                x = half - along
                y = half
                break
            case 2:
                x = -half
                y = half - along
                break
            case 3:
                x = -half + along
                y = -half
                break
        }

        return new Coord(x,y)

    }

    @Override
    Integer parse() {
        return input as int
    }

}