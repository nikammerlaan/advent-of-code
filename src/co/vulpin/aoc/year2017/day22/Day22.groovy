package co.vulpin.aoc.year2017.day22

import co.vulpin.aoc.Day

import java.awt.*

class Day22 extends Day<Map<Point, Integer>> {

    def part1() {
        def map = parse()
        def cur = new Point(0, 0)
        def dir = 0 // 0 = up, 1 = right, 2 = down, 3 = left
        def count = 0
        10_000.times {

            def val = map.get(cur) ?: 0

            switch(val) {
                case 0: dir--; break
                case 2: dir++; break
            }

            dir = validateValue(dir, 4)

            def newValue = (val + 2) % 4
            if(newValue == 2) count++

            map.put(new Point(cur), newValue)

            move(cur, dir)

        }

        return count
    }

    def part2() {
        def map = parse()
        def cur = new Point(0, 0)
        def dir = 0 // 0 = up, 1 = right, 2 = down, 3 = left
        def count = 0
        10_000_000.times {
            def val = map.get(cur) ?: 0

            switch(val) {
                case 0: dir--;    break
                case 2: dir++;    break
                case 3: dir += 2; break
            }

            dir = validateValue(dir, 4)

            def newValue = validateValue(val + 1, 4)
            if(newValue == 2) count++

            map.put(new Point(cur), newValue)

            move(cur, dir)

        }

        return count
    }

    void printMap(Map<Point, Integer> map, int size, Point cur) {
        (size..-size).each { y ->
            (-size..size).each { x ->
                def point = new Point(x, y)
                def value = map.get(point) ?: 0
                def c = "E"
                switch(value) {
                    case 0: c = "."; break
                    case 1: c = "W"; break
                    case 2: c = "#"; break
                    case 3: c = "F"; break
                }
                print point == cur ? "[$c]" : " $c "
            }
            println ""
        }
        println ""
    }

    int validateValue(int input, int max) {
        if(input < 0) input += max
        return input % max
    }

    void move(Point point, int dir) {
        point.with {
            switch(dir) {
                case 0: y++; break
                case 1: x++; break
                case 2: y--; break
                case 3: x--; break
            }
        }
    }

    Map<Point, Integer> parse() {
        def map = [:]
        def lines = input.split("\n")
        def centerX = lines.length / 2 as int
        def centerY = (lines[0].length() - 1) / 2 as int
        for(int row = 0; row < lines.length; row++) {
            def chars = lines[row].chars
            for(int col = 0; col < lines.length; col++) {
                def coord = new Point(col - centerX, (row - centerY) * -1)
                def value = chars[col] == "#" as char ? 2 : 0
                map.put(coord, value)
            }
        }
        return map
    }

}
