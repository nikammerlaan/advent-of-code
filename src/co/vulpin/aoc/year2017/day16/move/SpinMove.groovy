package co.vulpin.aoc.year2017.day16.move

class SpinMove extends Move {

    int amnt

    SpinMove(int amnt) {
        this.amnt = amnt
    }

    @Override
    void execute(List<Character> programs) {
        def copy = new ArrayList<Character>(programs)
        for(int i = 0; i < programs.size(); i++) {
            programs.set((i + amnt) % programs.size(), copy.get(i))
        }
    }

}
