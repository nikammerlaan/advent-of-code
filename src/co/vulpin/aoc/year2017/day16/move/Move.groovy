package co.vulpin.aoc.year2017.day16.move

abstract class Move {

    abstract void execute(List<Character> programs)

}
