package co.vulpin.aoc.year2017.day16.move

class PartnerMove extends Move {

    Character a, b

    PartnerMove(Character a, Character b) {
        this.a = a
        this.b = b
    }

    @Override
    void execute(List<Character> programs) {
        def aIndex = programs.findIndexOf { it == a }
        def bIndex = programs.findIndexOf { it == b }
        programs.swap(aIndex, bIndex)
    }

}
