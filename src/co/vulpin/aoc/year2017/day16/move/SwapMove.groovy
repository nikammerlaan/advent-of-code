package co.vulpin.aoc.year2017.day16.move

class SwapMove extends Move {

    int a, b

    SwapMove(int a, int b) {
        this.a = a
        this.b = b
    }

    @Override
    void execute(List<Character> programs) {
        programs.swap(a, b)
    }


}
