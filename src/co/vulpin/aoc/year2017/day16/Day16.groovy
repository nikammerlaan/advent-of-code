package co.vulpin.aoc.year2017.day16

import co.vulpin.aoc.Day
import co.vulpin.aoc.year2017.day16.move.Move
import co.vulpin.aoc.year2017.day16.move.PartnerMove
import co.vulpin.aoc.year2017.day16.move.SpinMove
import co.vulpin.aoc.year2017.day16.move.SwapMove

class Day16 extends Day<List<Move>> {

    void calculate() {

        def programs = ('a'..'p').collect{ it } as List<Character>
        def original = programs.join("")

        dance(programs)

        part1.complete(programs.join(""))

        def iterations = [original, programs.join("")]
        def cycle = 1

        while(iterations.last() != original) {
            dance(programs)
            iterations += programs.join("")
            cycle++
        }

        def index = 1_000_000_000 % cycle
        part2.complete(iterations[index])

    }

    void dance(List<Character> programs) {
        parsed.each { it.execute(programs) }
    }

    @Override
    List<Move> parse() {
        return input.split(",").collect {
            def type = it.charAt(0)
            def rest = it.substring(1)
            switch(type) {
                case 's':
                    return new SpinMove(rest as int)
                case 'x':
                    def splits = rest.split("/")
                    return new SwapMove(splits[0] as int, splits[1] as int)
                case 'p':
                    def splits = rest.split("/")
                    return new PartnerMove(splits[0] as char, splits[1] as char)
            }
        }
    }

}
