package co.vulpin.aoc.year2017.day13

import co.vulpin.aoc.Day
import groovy.transform.Memoized

class Day13 extends Day<Map<Integer, Integer>> {

    def part1() {
        def caught = travel(0)
        return caught.collect{ it * parsed.get(it) }.sum() as int
    }

    def part2() {
        def start = parsed.keySet().min()
        def end = parsed.keySet().max()

        def caught = true
        def delay

        for(delay = 0; caught; delay++) {
            caught = false

            for(int i = start; i <= end; i++) {
                def depth = parsed.get(i)
                if(depth) {
                    def scannerPos = getScannerPos(depth, i + delay)
                    if(scannerPos == 0) {
                        caught = true
                        break
                    }
                }
            }

        }

        return delay
    }

    @Memoized
    static int getScannerPos(int depth, int second) {
        def positions = (0..depth -1) + (depth - 2..1)
        def remainder = second % positions.size()
        return positions[remainder]
    }

    @Memoized
    static List<Integer> travel(int delay) {
        def map = parse().clone() as Map<Integer, Integer>

        def caught = []

        def start = map.keySet().min()
        def end = map.keySet().max()

        (start..end).each { position ->
            def depth = map.get(position)
            if(depth) {
                def scannerPos = getScannerPos(depth, position + delay)
                if(scannerPos == 0) {
                    caught += position
                }
            }
        }

        return caught
    }

    @Override
    Map<Integer, Integer> parse() {
        def map = [:]
        input.split("\n").each {
            def parts = it.split(": ")
            map.put(parts[0] as int, parts[1] as int)
        }
        return map
    }

}
