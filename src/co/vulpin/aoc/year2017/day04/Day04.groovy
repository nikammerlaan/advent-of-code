package co.vulpin.aoc.year2017.day04

import co.vulpin.aoc.Day

class Day04 extends Day<String[][]> {

    def part1() {
        return parsed
                .findAll{ !containsDuplicates(it) }
                .size()
    }

    def part2() {
        return parsed.collect{ phrase ->
            phrase.collect { word ->
                def chars = word.chars
                Arrays.sort(chars)
                return new String(chars)
            } as String[]
        }.findAll{ !containsDuplicates(it) }.size()
    }

    static boolean containsDuplicates(String[] words) {
        def set = new HashSet<>(Arrays.asList(words))
        return words.size() != set.size()
    }

    String[][] parse() {
       return input.split("\n").collect{ it.split("\\s+") }
    }

}
