package co.vulpin.aoc.year2017.day10

import co.vulpin.aoc.Day
import co.vulpin.aoc.misc.KnotHash

class Day10 extends Day<List<Integer>> {

    def part1() {
        def nums = new ArrayList<Integer>(0..255)

        KnotHash.knot(nums, parsed, 1)

        return nums[0] * nums[1]
    }

    def part2() {
        return KnotHash.knot(input)
    }

    @Override
    List<Integer> parse() {
        return input.split(",").collect{ it as int }
    }

}
