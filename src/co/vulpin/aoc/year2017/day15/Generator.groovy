package co.vulpin.aoc.year2017.day15

class Generator {

    int factor
    long value
    long start

    Generator(int factor, int start) {
        this.factor = factor
        this.value = start
        this.start = start
    }

    int nextValue() {
        value *= factor
        value %= Integer.MAX_VALUE
        return value
    }

    void reset() {
        value = start
    }

}
