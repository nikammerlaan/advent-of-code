package co.vulpin.aoc.year2017.day15

import co.vulpin.aoc.Day

class Day15 extends Day<Map<Character, Generator>> {

    static Generator genA = new Generator(16_807, 699)
    static Generator genB = new Generator(48_271, 124)

    def part1() {
        def matches = 0

        40_000_000.times {
            def a = genA.nextValue()
            def b = genB.nextValue()

            if(bin(a) == bin(b)) matches++
        }

        return matches
    }

    def part2() {
        genA.reset()
        genB.reset()

        def matches = 0

        5_000_000.times {

            def a
            while(true) {
                a = genA.nextValue()
                if(a % 4 == 0) break
            }

            def b
            while(true) {
                b = genB.nextValue()
                if(b % 8 == 0) break
            }

            if(bin(a) == bin(b)) matches++

        }

        return matches
    }

    static String bin(int number) {
        def binary = Integer.toBinaryString(number)
        binary = "0" * 16 + binary
        return binary.substring(binary.size() - 16)
    }

}
