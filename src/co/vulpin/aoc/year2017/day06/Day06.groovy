package co.vulpin.aoc.year2017.day06

import co.vulpin.aoc.Day

class Day06 extends Day<List<Integer>> {

    def part1() {
        HashMap<List<Integer>, Integer> seen = [:]

        def current = parse()

        def count = 0

        while(!seen.containsKey(current)) {
            seen.put(current, count)
            distribute(current)
            count++
        }

        return count
    }

    def part2() {
        HashMap<List<Integer>, Integer> seen = [:]

        def current = parse()

        def count = 0

        while(!seen.containsKey(current)) {
            seen.put(current, count)
            distribute(current)
            count++
        }

        return count - seen.get(current)
    }

    static List<Integer> distribute(List<Integer> banks) {
        def max = banks.max()
        def maxIndex = banks.indexOf(max)

        banks[maxIndex] = 0

        for(int i = maxIndex + 1; max > 0; i++) {
            i %= banks.size()
            banks.set(i, banks.get(i) + 1)
            max--
        }

        return banks
    }

    @Override
    List<Integer> parse() {
        input.split("\\s+").collect{ it as int }
    }

}
